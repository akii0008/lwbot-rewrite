const Discord = require('discord.js');

module.exports.run = async (client, message, args) => {
  const GuildSettings = require('../../dbFunctions/message/settings');
  const settings = new GuildSettings(message.guild.id);
  const toKick = message.mentions.users.first();
  const toKickM = message.mentions.members.first();
  const reason = args.slice(1).join(' ');

  if (!message.guild.me.permissions.has('MOVE_MEMBERS')) return message.send('❌ `|` 👢 **I am missing permissions:** `Move Members`');
  if (!message.guild.me.permissions.has('MANAGE_CHANNELS')) return message.send('❌ `|` 👢 **I am missing permissions:** `Manage Channels` ');
  if (!message.member.permissions.has('KICK_MEMBERS')) return message.send('❌ `|` 👢 **You are missing permissions:** `Kick Members`');
  if (!toKick) return message.send('❌ `|` 👢 **You didn\'t mention someone to voicekick!**');
  if (!message.member.voice.channel) return message.send('❌ `|` 👢 **You are not in the voice channel!**');
  if (!toKickM.voice.channel) return message.send(`❌ \`|\` 👢 ${toKick.toString()} **isn't in a voice channel!**`);
  if (!toKickM.voice.channel === message.member.voice.channel) return message.send(`❌ \`|\` 👢 **You must be in the same voice channel as** ${toKick.toString()}`);

  await message.guild.modbase.create({
    victim: toKick.id,
    moderator: message.author.id,
    type: 'voicekick'
  }).then(async info => {
    let dmMsg = `👢 **You were voicekicked from** \`${message.member.voice.channel.name}\`, **in** \`${message.guild.name}\` \`|\` 👢 **Responsible Moderator:** ${message.author.toString()} (${message.author.tag})`;

    const modEmbed = new Discord.MessageEmbed()
      .setTitle('Member Voicekicked')
      .setThumbnail(toKick.displayAvatarURL({ format: 'png', dynamic: true }))
      .setColor('0xA80000')
      .setFooter(`ID: ${toKick.id} | Case: ${info.id}`)
      .addField('Voicekicked Member', `${toKick.toString()} (${toKick.tag})`)
      .addField('Moderator', `${message.author.toString()} (${message.author.tag})`)
      .addField('Channel:', message.member.voice.channel.name);

    if (reason) { dmMsg += `\n\n⚙️ **Reason: \`${reason}\`**`; modEmbed.addField('Reason', reason); message.guild.modbase.update({ reason: reason }, { where: { id: info.id } }); }

    toKickM.voice.kick(reason ? reason : null);
    toKick.send(dmMsg);
    await settings.get('modLogChannel')
      .then(async modLogChannel => {
        modLogChannel = message.guild.channels.cache.find(g => g.name.toLowerCase() === modLogChannel.toLowerCase());
        if (!modLogChannel) return message.send(`⚠️ **Voicekick completed, but there is no mod log channel set.** Try \`${await settings.get('prefix')}set <edit/add> modLogChannel <channel name>\``);
        if (!message.guild.me.permissionsIn(modLogChannel).serialize()['SEND_MESSAGES'] || !message.guild.me.permissionsIn(modLogChannel).serialize()['EMBED_LINKS']) {
          modLogChannel.createOverwrite(client.user, { SEND_MESSAGES: true, EMBED_LINKS: true }).catch(() => { return message.send(`⚠️ **Ban completed, but I errored:**\nI tried to give myself permissions to send messages or post embeds in ${modLogChannel}, but I couldn't. Please make sure I have the \`Manage Roles\` permission, as that allows me to.`); });
        }
        await modLogChannel.send(modEmbed);
        await message.send(`✅ \`|\` 👢 **Voicekicked user \`${toKick.tag}\`**`);
      })
      .catch(async e => message.send(`❌ **There was an error finding the mod log channel:** \`${e.stack}\``));
  });
};

exports.conf = {
  enabled: true,
  guildOnly: true,
  aliases: [],
  permLevel: 'Moderator'
};

exports.help = {
  name: 'voicekick',
  description: 'Kick someone from the current voice channel',
  usage: 'voicekick <@user> [reason]',
  category: 'Moderation'
};