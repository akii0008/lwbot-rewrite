module.exports.run = async (client, message, args) => {
  const xpSchema = require('../../dbFunctions/message/xp').functions.xpSchema(message.guild.id);

  xpSchema.findAll({
    order: [
      ['xp', 'DESC'],
      ['id', 'DESC']
    ]
  }).then(async data => {

    let combined = '';

    for (const values of data) {
      const member = await message.guild.members.fetch(values.dataValues.user);

      // this member is in first place on the guild ranking
      if (data.indexOf(values) === 0) {
        if (member === undefined)
          combined += `👑 [User Left]\n# XP: ${values.dataValues.xp} - Level: ${values.dataValues.level}\n\n`;
        else
          combined += `👑 ${member.user.tag}\n# XP: ${values.dataValues.xp} - Level: ${values.dataValues.level}\n\n`;
      }

      if (data.indexOf(values) <= 9 && data.indexOf(values) >= 1) {
        if (member === undefined)
          combined += `${data.indexOf(values) + 1}. [User Left]\n# XP: ${values.dataValues.xp} - Level: ${values.dataValues.level}\n\n`;
        else
          combined += `${data.indexOf(values) + 1}. ${member.user.tag}\n# XP: ${values.dataValues.xp} - Level: ${values.dataValues.level}\n\n`;
      }

    }

    message.send(combined, { code: 'markdown' }).then(async msg => {
      if (data.length < 10) return; // There isn't enough for more than one page. Stop here.

      await msg.react('◀');
      await client.wait(300);
      await msg.react('🛑');
      await client.wait(300);
      await msg.react('▶');

      let min = 0;
      let max = 9;
      let curPage = args[1] || 1;

      if (curPage !== 1) {
        max = curPage * 10 - 1;
        min = max - 9;
      }

      const filter = (reaction, user) => ['◀', '🛑', '▶'].includes(reaction.emoji.name) && user.id === message.author.id;
      const collector = msg.createReactionCollector(filter, { time: 120000 })
        .on('collect', async g => {
          if (g._emoji.name === '🛑') return collector.emit('end');

          else if (g._emoji.name === '◀') {
            if (min === 0 || curPage === 0) return msg.reactions.cache.get('◀').users.remove(message.author);

            await client.wait(300);
            msg.reactions.cache.get('◀').users.remove(message.author);

            min = min - 10;
            max = max - 10;
            curPage--;

            let combined = '';
            for (const values of data) {
              const member = await message.guild.members.fetch(values.dataValues.user);

              if (data.indexOf(values) < min) continue;

              if (member === undefined)
                combined += `${data.indexOf(values) + 1}. [User Left]\n# XP: ${values.dataValues.xp} - Level: ${values.dataValues.level}\n\n`;
              else
                combined += `${data.indexOf(values) + 1}. ${member.user.tag}\n# XP: ${values.dataValues.xp} - Level: ${values.dataValues.level}\n\n`;

              if (data.indexOf(values) >= max) break;
            }

            msg.edit(combined, { code: 'markdown' });
          } else if (g._emoji.name === '▶') {

            await client.wait(300);
            msg.reactions.cache.get('▶').users.remove(message.author);

            min = min + 10;
            max = max + 10;
            curPage++;

            if (curPage > Math.ceil(data.length / 10)) {
              curPage--;
              min = min - 10;
              max = max - 10;
              message.send('**This is the final page. There is no more data past this range.**').then(msg => msg.delete({ timeout: 5000 }));
              return;
            }

            let combined = '';
            for (const values of data) {
              const index = data.indexOf(values);
              if (index < min) continue;

              const member = await message.guild.members.fetch(values.dataValues.user);
              if (member === undefined)
                combined += `${data.indexOf(values) + 1}. [User Left]\n# XP: ${values.dataValues.xp} - Level: ${values.dataValues.level}\n\n`;
              else
                combined += `${data.indexOf(values) + 1}. ${member.user.tag}\n# XP: ${values.dataValues.xp} - Level: ${values.dataValues.level}\n\n`;
            }

            msg.edit(combined, { code: 'markdown' });
          }

        }).on('end', () => {
          msg.reactions.removeAll();
        });

    });
  });
};

exports.conf = {
  enabled: true,
  permLevel: 'User',
  guildOnly: true,
  aliases: ['leaderboard', 'xplevels', 'xpleaderboard']
};

exports.help = {
  name: 'levels',
  description: 'Get the server\'s leaderboard',
  usage: 'levels',
  category: 'Server'
};