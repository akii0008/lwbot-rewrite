const GuildSARs = require('../../dbFunctions/message/sar');

module.exports.run = async (client, message, role) => {
  const sar = new GuildSARs(message.guild.id);

  if (role.length === 0) return message.send('❌ `|` 📋 **You didn\'t give me a role to add!**');
  role = role.join(' ');

  console.log(role);
  try { await message.functions.parseRole(role); } catch (e) { return message.send(`❌ \`|\` 📋 **${e}**`); }
  const sarRole = await message.functions.parseRole(role);
  console.log(sarRole);
  
  if(sarRole.managed) return message.send('❌ `|` 📋 **That role is managed by an integration and cannot be manually added!**');

  await sar.addRole(sarRole.id);

  message.send(`✅ \`|\` 📋 **Added role** \`${sarRole.name}\``);
};

exports.conf = {
  enabled: true,
  aliases: ['addselfassignablerole'],
  guildOnly: true,
  permLevel: 'User'
};

exports.help = {
  name: 'asar',
  description: 'Add a self-assignable role to this server',
  usage: 'asar <role/role ID>',
  category: 'Server'
};