const Tags = require('../../dbFunctions/client/tags').tagsTable;

module.exports.run = async (client, message, args) => {

  const tagName = args.shift();
  const tagDescription = args.join(' ');

  // equivalent to: UPDATE tags (descrption) values (?) WHERE name='?';
  const affectedRows = await Tags.update({ description: tagDescription }, { where: { name: tagName } });
  if (affectedRows > 0) {
    return message.send(`✅ \`|\` :pencil: **\`${tagName}\` edited.**`);
  }
  return message.send(`❌ \`|\` :pencil: **\`${tagName}\` does not exist**`);
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ['etag'],
  permLevel: 'Bot Support'
};

exports.help = {
  name: 'edittag',
  description: 'Edits a particular tag',
  usage: 'edittag <tag name> <new tag data>',
  category: 'Tags'
};