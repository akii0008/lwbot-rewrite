const fetch = require('node-fetch');

module.exports.run = (client, message, args) => {
  if (!args[0]) return message.send(':x: `|` 💡 **Missing expression to evaulate!**');

  const math = args.join(' ');

  const body = JSON.stringify({
    expr: math
  });

  fetch('http://api.mathjs.org/v4/', {
    method: 'post',
    body,
    headers: { 'Content-Type': 'application/json' }
  }).then(res => res.json())
    .then(res => {
      if(res.result === null) return message.send(':x: `|` 💡 **Error evaluating expression.**');
      message.send(`:bulb: **Evaluated expression:**\n\`\`\`js\n${res.result}\n\`\`\``);
    }).catch(e => {
      message.send(`:x: \`|\` 💡 **Error evaulating expression:**\n\`\`\`\n${e}\n\`\`\``);
      client.logger.error(e);
    });
};

exports.conf = {
  enabled: true,
  aliases: [],
  permLevel: 'User',
  guildOnly: false
};

exports.help = {
  name: 'math',
  description: 'Evaluate a mathematical expression',
  usage: 'math <expression>',
  category: 'Misc'
};