const fetch = require('node-fetch');
const { MessageEmbed } = require('discord.js');
module.exports.run = (client, message, args) => {
  const search = args[0];

  if (!search) { // Get the latest xkcd comic
    fetch('https://xkcd.com/info.0.json')
      .then(async res =>{
        const json = await res.json();
        displayXKCD(json);
      });
  } else {
    if (isNaN(search)) return message.send(`❌ \`|\` :mag: \`${search}\` **is not a number!**`);
    fetch(`https://xkcd.com/${search}/info.0.json`)
      .then(async res => {
        if (!res.ok) return message.send(`❌ \`|\` :mag: **XKCD **\`#${search}\` **does not exist.**`);
        const json = await res.json();
        displayXKCD(json, +search);
      });
  }

  function displayXKCD(json, number) {
    if(!number) number = json.num;

    const embed = new MessageEmbed()
      .setColor(client.config.colors.white)
      .setTitle(json.safe_title)
      .setDescription(json.alt)
      .setImage(json.img)
      .setFooter(`#${json.num} • ${json.month}/${json.day}/${json.year}`)
      .setAuthor('xkcd', 'https://i.imgur.com/XcDZksp.png', `https://xkcd.com/${json.num}`);

    message.send(embed).then(async msg => {
      await msg.react('◀');
      await client.wait(300);
      await msg.react('🛑');
      await client.wait(300);
      await msg.react('▶');

      const filter = (reaction, user) => ['◀', '🛑', '▶'].includes(reaction.emoji.name) && user.id === message.author.id;
      const collector = msg.createReactionCollector(filter, { time: 120000 })
        .on('collect', async g => {
          if (g._emoji.name === '🛑') return collector.emit('end');
          else if (g._emoji.name === '◀') {
            if (number === 1) return msg.reactions.cache.get('◀').users.remove(message.author);
            msg.reactions.cache.get('◀').users.remove(message.author);
            number--;
            fetch(`https://xkcd.com/${+number}/info.0.json`)
              .then(async res => {
                if (!res.ok) return msg.edit(`❌ \`|\` :mag: **XKCD **\`#${number}\` **does not exist.**`, { embed: null });
                const json = await res.json();
                msg.edit('', new MessageEmbed()
                  .setColor(client.config.colors.white)
                  .setTitle(json.safe_title)
                  .setDescription(json.alt)
                  .setImage(json.img)
                  .setFooter(`#${json.num} • ${json.month}/${json.day}/${json.year}`)
                  .setAuthor('xkcd', 'https://i.imgur.com/XcDZksp.png', `https://xkcd.com/${json.num}`));
              });

          } else if (g._emoji.name === '▶') {
            msg.reactions.cache.get('▶').users.remove(message.author);
            number++;
            fetch(`https://xkcd.com/${+number}/info.0.json`)
              .then(async res => {
                if (!res.ok) return msg.edit(`❌ \`|\` :mag: **XKCD **\`#${number}\` **does not exist.**`, { embed: null });
                const json = await res.json();
                msg.edit('', new MessageEmbed()
                  .setColor(client.config.colors.white)
                  .setTitle(json.safe_title)
                  .setDescription(json.alt)
                  .setImage(json.img)
                  .setFooter(`#${json.num} • ${json.month}/${json.day}/${json.year}`)
                  .setAuthor('xkcd', 'https://i.imgur.com/XcDZksp.png', `https://xkcd.com/${json.num}`));
              });
          } else msg.reactions.cache.get(g._emoji.name).users.remove(message.author);

        })
        .on('end', () => msg.reactions.removeAll());
    });
  }
};

exports.conf = {
  enabled: true,
  permLevel: 'User',
  guildOnly: false,
  aliases: [],
  requiresEmbed: true
};

exports.help = {
  name: 'xkcd',
  description: 'Find an XKCD',
  usage: 'xkcd [#]',
  category: 'Comics'
};