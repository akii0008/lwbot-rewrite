const { MessageEmbed } = require('discord.js');
const moment = require('moment');
module.exports.run = (client, message) => {
  const voiceChannel = message.member.voice.channel;
  if (!voiceChannel) return message.send('❌ `|` 🎵 **You aren\'t in a voice channel!**');

  const music = client.musicQueue.get(message.guild.id);
  if (!music) return message.send('❌ `|` 🎵 **There\'s nothing playing!**');

  // This is quite an edge case but sometimes the a user tries to find the current song between songs.
  // This is when the bot is still connected but there is no dispatcher because there is no song playing
  // because it is still being loaded.
  // This handles that edge case.
  if (music.connection && !music.connection.dispatcher) return message.send('❌ `|` 🎵 **Please wait! I\'m currently loading a new song!**');

  let progressBar = '';
  const percentage = music.connection.dispatcher.streamTime / music.songs[0].duration; // DO NOT MULTIPLY THIS BY 100! IT'S IN DECIMAL FORM FOR A REASON!
  const section1 = Math.ceil(20 * percentage);
  const section2 = Math.floor(20 * (1 - percentage));
  progressBar += `${'▬'.repeat(section1)}🔘${'▬'.repeat(section2)}`;

  const timeElapsed = moment.duration(music.playing.duration, 'seconds').format('H[:]mm[:]ss');
  const totalTime = moment.duration(music.songs[0].duration, 'milliseconds').format('H[:]mm[:]ss');

  message.send(new MessageEmbed()
    .setColor(client.accentColor)
    .setTitle(music.songs[0].title)
    .setURL(music.songs[0].url)
    .setThumbnail(music.songs[0].thumbnail)
    .setDescription(`${timeElapsed} ${progressBar} ${totalTime}`)
  );
};

exports.conf = {
  enabled: false,
  aliases: ['np', 'playing', 'whatisthissong'],
  permLevel: 'User',
  guildOnly: true,
  requiresEmbed: true,
  failoverDisabled: true
};

exports.help = {
  name: 'nowplaying',
  description: 'Shows what song is currently playing',
  usage: 'nowplaying',
  category: 'Music'
};