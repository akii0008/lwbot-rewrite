const deck = [
  // RED
  { suit: 'red', value: 0 },
  { suit: 'red', value: 1 }, { suit: 'red', value: 1 },
  { suit: 'red', value: 2 }, { suit: 'red', value: 2 },
  { suit: 'red', value: 3 }, { suit: 'red', value: 3 },
  { suit: 'red', value: 4 }, { suit: 'red', value: 4 },
  { suit: 'red', value: 5 }, { suit: 'red', value: 5 },
  { suit: 'red', value: 6 }, { suit: 'red', value: 6 },
  { suit: 'red', value: 7 }, { suit: 'red', value: 7 },
  { suit: 'red', value: 8 }, { suit: 'red', value: 8 },
  { suit: 'red', value: 9 }, { suit: 'red', value: 9 },
  { suit: 'red', value: 'drawtwo' }, { suit: 'red', value: 'drawtwo' },
  { suit: 'red', value: 'skip' }, { suit: 'red', value: 'skip' },
  { suit: 'red', value: 'reverse' }, { suit: 'red', value: 'reverse' },

  // BLUE
  { suit: 'blue', value: 0 },
  { suit: 'blue', value: 1 }, { suit: 'blue', value: 1 },
  { suit: 'blue', value: 2 }, { suit: 'blue', value: 2 },
  { suit: 'blue', value: 3 }, { suit: 'blue', value: 3 },
  { suit: 'blue', value: 4 }, { suit: 'blue', value: 4 },
  { suit: 'blue', value: 5 }, { suit: 'blue', value: 5 },
  { suit: 'blue', value: 6 }, { suit: 'blue', value: 6 },
  { suit: 'blue', value: 7 }, { suit: 'blue', value: 7 },
  { suit: 'blue', value: 8 }, { suit: 'blue', value: 8 },
  { suit: 'blue', value: 9 }, { suit: 'blue', value: 9 },
  { suit: 'blue', value: 'drawtwo' }, { suit: 'blue', value: 'drawtwo' },
  { suit: 'blue', value: 'skip' }, { suit: 'blue', value: 'skip' },
  { suit: 'blue', value: 'reverse' }, { suit: 'blue', value: 'reverse' },

  // GREEN
  { suit: 'green', value: 0 },
  { suit: 'green', value: 1 }, { suit: 'green', value: 1 },
  { suit: 'green', value: 2 }, { suit: 'green', value: 2 },
  { suit: 'green', value: 3 }, { suit: 'green', value: 3 },
  { suit: 'green', value: 4 }, { suit: 'green', value: 4 },
  { suit: 'green', value: 5 }, { suit: 'green', value: 5 },
  { suit: 'green', value: 6 }, { suit: 'green', value: 6 },
  { suit: 'green', value: 7 }, { suit: 'green', value: 7 },
  { suit: 'green', value: 8 }, { suit: 'green', value: 8 },
  { suit: 'green', value: 9 }, { suit: 'green', value: 9 },
  { suit: 'green', value: 'drawtwo' }, { suit: 'green', value: 'drawtwo' },
  { suit: 'green', value: 'skip' }, { suit: 'green', value: 'skip' },
  { suit: 'green', value: 'reverse' }, { suit: 'green', value: 'reverse' },

  // YELLOW
  { suit: 'yellow', value: 0 },
  { suit: 'yellow', value: 1 }, { suit: 'yellow', value: 1 },
  { suit: 'yellow', value: 2 }, { suit: 'yellow', value: 2 },
  { suit: 'yellow', value: 3 }, { suit: 'yellow', value: 3 },
  { suit: 'yellow', value: 4 }, { suit: 'yellow', value: 4 },
  { suit: 'yellow', value: 5 }, { suit: 'yellow', value: 5 },
  { suit: 'yellow', value: 6 }, { suit: 'yellow', value: 6 },
  { suit: 'yellow', value: 7 }, { suit: 'yellow', value: 7 },
  { suit: 'yellow', value: 8 }, { suit: 'yellow', value: 8 },
  { suit: 'yellow', value: 9 }, { suit: 'yellow', value: 9 },
  { suit: 'yellow', value: 'drawtwo' }, { suit: 'yellow', value: 'drawtwo' },
  { suit: 'yellow', value: 'skip' }, { suit: 'yellow', value: 'skip' },
  { suit: 'yellow', value: 'reverse' }, { suit: 'yellow', value: 'reverse' },

  // SPECIAL
  { suit: 'special', value: 'wild' }, { suit: 'special', value: 'wild' },
  { suit: 'special', value: 'wild' }, { suit: 'special', value: 'wild' },

  { suit: 'special', value: 'wild4' }, { suit: 'special', value: 'wild4' },
  { suit: 'special', value: 'wild4' }, { suit: 'special', value: 'wild4' }
];
// * Four suits: Red, Green, Yellow and Blue, each consisting of one 0 card, two 1 cards, two 2s, 3s, 4s, 5s, 6s, 7s, 8s, 9s
// * two Draw Two cards, two Skip cards, and two Reverse cards
// * four Wild cards and four Wild Draw Four cards. 

module.exports.run = (client, message, args) => {
  if (args.length === 0)
    return message.send('❌ `|` 🃏 **You didn\'t say what to do!** Possible options: `start`, `end`, `join`, or if you\'re in a game, `uno`');
  if (!['start', 'end', 'join', 'uno'].includes(args[0]))
    return message.send('❌ `|` 🃏 **Invalid option!** Possible options: `start`, `end`, `join`, `uno`');

  if (args[0] === 'start') {
    if (message.guild.uno) {
      if (message.guild.uno.author !== message.author.id)
        return message.send('❌ `|` 🃏 **There\'s already a game of Uno in progress in this guild!**');

      //if (message.guild.uno.players.length === 1)
      //  return message.send('❌ `|` 🃏 **You can\'t start a game with only yourself!**');

      else {
        message.send(`✅ \`|\` 🃏 **Game started with ${message.guild.uno.players.length} players!**`);
        return startGame();
      }
    }

    message.guild.uno = {
      players: [{ id: message.author.id, hand: [] }],
      author: message.author.id,
      started: false,
      plays: 0,
      lastCard: { suit: undefined, value: undefined, wild: undefined, wild4: undefined },
      duration: 10000 // 10 seconds
    };
    return message.send(`✅ \`|\` 🃏 **Game initialized!** ${message.author.toString()}, when everyone is ready, type \`${message.guild.settings.prefix}uno start\` again to begin!\nEveryone else, type \`${message.guild.settings.prefix}uno join\` to join the game!`);
  }

  if (args[0] === 'end') {
    if (!message.guild.uno)
      return message.send('❌ `|` 🃏 **There isn\'t currently a game of Uno in progress in this guild!**');
    if (message.guild.uno.author !== message.author.id)
      return message.send('❌ `|` 🃏 **You cannot end this game because you didn\'t start it!**');

    return endGame();
  }

  if (args[0] === 'join') {
    if (!message.guild.uno)
      return message.send('❌ `|` 🃏 **There isn\'t currently a game of Uno in progress in this guild!**');
    if (message.guild.uno.players.find(g => g.id === message.author.id))
      return message.send('❌ `|` 🃏 **You\'ve already joined!**');
    if (message.guild.uno.players.length === 10)
      return message.send('❌ `|` 🃏 **There are already 10 people in this group!**');

    message.guild.uno.players.push({ id: message.author.id, hand: [] });
    return message.send(`✅ \`|\` 🃏 **You have joined the game!** Players: \`${message.guild.uno.players.length}\``);
  }

  async function startGame() {
    // If for some reason there aren't enough players, stop the game before it begins.
    if(message.guild.uno.players.length < 1) {
      endGame('not enough players');
    }

    const shuffledDeck = shuffle(deck);
    const startingCard = shuffledDeck.shift();

    if (startingCard.suit === 'special' || startingCard.value === 'drawtwo' || startingCard.value === 'skip' || startingCard.value === 'reverse') {
      // Try again. The game didn't start "properly"
      return startGame();
    }

    message.guild.uno.players.forEach(async player => {
      for (let i = 0; i < 6; i++) { // 7 times, 0-6
        const randomCard = shuffledDeck.shift();
        player.hand.push(randomCard);
      }
      (await client.users.fetch(player.id))
        .send(`🃏 **Here's your hand!**\n${player.hand.map(card => `**\`${card.suit.toUpperCase()} ${card.value}\`**`).join(', ')}\n*You can play a card like this:* \`red 4\`\n*If you have a wild card, play it like this:* \`wild blue\` or \`wild4 blue\``)
        .catch(async e => {
          if (e.message === 'Cannot send messages to this user') {
            message.guild.uno.players.splice(message.guild.uno.players.indexOf(player), 1); // Remove the player from the queue
            message.send(`❌ \`|\` 🃏 ${(await client.users.fetch(player.id)).toString()}, **I had to remove you from the game.**\nPlease enable your DMs so I can send you your cards, and rejoin later.`);
          } else {
            endGame();
            client.logger.error(e);
            message.send('❌ 🃏 **There was an error!** Please try again later.');
          }
        });
    });

    const currentPlayer = message.guild.uno.players[0];
    const response = await awaitReply(message, `🃏 **Here we go!**\nI'll start with a card: ${formatCard(startingCard)}\nHey ${(await client.users.fetch(currentPlayer.id)).toString()}! **It's your turn!**\n(You have 15 seconds.)`, 15000, currentPlayer);

    return game(response, currentPlayer, startingCard, shuffledDeck);
  }

  async function game(response, currentPlayer, lastCard, remainingDeck) {
    message.guild.uno.lastPlay = '';
    response = parseResponse(response); // null | false | card object
    const playerUserObject = client.users.fetch(currentPlayer.id);

    /****** BEGIN RULES SECTION ******/

    if (response === null) { // Timed out
      message.send(`❌ \`|\` 🃏 **Time's up, ${playerUserObject.toString()}!**`);
      message.guild.uno.lastPlay = 'invalid';
    }
    else if (response === false) {
      message.send(`❌ \`|\` 🃏 **That play wasn't valid, ${playerUserObject.toString()}!**`);
      message.guild.uno.lastPlay = 'invalid';
    } 
    
    else if(response === 'draw') {
      message.send(`🃏 **${playerUserObject.toString()} drew a card!**`);
      const [newHand, newDeck] = addCards(message.guild.uno.players[0], 1, remainingDeck);
      remainingDeck = newDeck;
      message.guild.uno.players[0].hand = newHand;
      playerUserObject.send(`🃏 **Here's your hand!**\n${currentPlayer.hand.map(card => `**\`${card.suit.toUpperCase()} ${card.value}\`**`).join(', ')}\n*You can play a card like this:* \`red 4\`\n*If you have a wild card, play it like this:* \`wild blue\` or \`wild4 blue\``).catch(async e => {
        if (e.message === 'Cannot send messages to this user') {
          message.guild.uno.players.splice(message.guild.uno.players.indexOf(currentPlayer), 1); // Remove the player from the queue
          message.send(`❌ \`|\` 🃏 ${(await client.users.fetch(currentPlayer.id)).toString()}, **I had to remove you from the game.**\nPlease enable your DMs so I can send you your cards, and rejoin later.`);
        } else {
          endGame('error');
          client.logger.verbose(`From: ${__filename}`);
          client.logger.error(e);
          message.send('❌ 🃏 **There was an error!** Please try again later.');
        }
      });

      message.guild.uno.lastPlay = 'draw';
    }

    else if(!currentPlayer.hand.find(g => response.suit === g.suit && response.value === g.value)) {
      console.log('Current player hand:', currentPlayer.hand);
      console.log('Response:', response);

      message.send(`❌ \`|\` 🃏 **You don't have that card in your hand, ${playerUserObject.toString()}!**`);
      message.guild.uno.lastPlay = 'invalid';
    }

    else if(response.wild) {
      message.send(`🎨 \`|\` 🃏 **Wild card!** The color is now **${response.suit}**.`);
      lastCard.suit = response.suit;

      const _currentPlayer = message.guild.uno.players.shift();
      message.guild.uno.players.push(_currentPlayer);

      message.guild.uno.lastPlay = 'wild';
    }

    else if(response.wild4) {
      message.send(`🎨 \`|\` 🃏 **Draw four, ${await client.users.fetch(message.guild.uno.players[1].id)}!** The color is now **${response.suit}**.`);
      lastCard.suit = response.suit;

      const [newHand, newDeck] = addCards(message.guild.uno.players[1], 4, remainingDeck);
      message.guild.uno.players[1].hand = newHand;
      remainingDeck = newDeck;
      (await client.users.fetch(message.guild.uno.players[1].id)).send(`🃏 **Here's your hand!**\n${message.guild.uno.players[1].hand.map(card => `**\`${card.suit.toUpperCase()} ${card.value}\`**`).join(', ')}\n*You can play a card like this:* \`red 4\`\n*If you have a wild card, play it like this:* \`wild blue\` or \`wild4 blue\``).catch(catchDMAndRemove);

      // SEE SKIP FUNCTIONALITY FOR WHY THIS WORKS
      const _currentPlayer = message.guild.uno.players.shift();
      message.guild.uno.players.push(_currentPlayer);
      // Skipped player isnt second in the queue anymore, they're first because the current player had to be looped back.
      const skippedPlayer = message.guild.uno.players.splice(0, 1)[0]; // .splice(0, 1) returns an array consisting of exactly 1 player.
      message.guild.uno.players.push(skippedPlayer);

      message.guild.uno.lastPlay = 'wild4';
    }

    else if (response.suit !== lastCard.suit && response.value !== lastCard.value) {
      message.send(`❌ \`|\` 🃏 **That play wasn't valid, ${playerUserObject.toString()}!**\nYou played a **${response.suit} ${response.value}** card, and the last card was a **${lastCard.suit} ${lastCard.value}** card.`);
      message.guild.uno.lastPlay = 'invalid';
    }

    else if(response.value === 'skip') {
      message.send(`⏩ \`|\` 🃏 **Skipped ${await client.users.fetch(message.guild.uno.players[1].id)}!**`);

      // SKIP FUNCTIONALITY:
      /*    Starting out: *//*  

            +--- Current player
            |   +--- Player to skip
            |   |   
            v   v   
          [p1, p2, p3, p4]

          *//* Shift and push p1: *//*
            
            +--- Player to skip
            |           +--- Current player
            |           |
            v           v
          [p2, p3, p4, p1]

          *//* Shift and push p2: *//*

                    +--- Current player
          (next)    |   +--- Skipped player
            |       |   |
            v       v   v
          [p3, p4, p1, p2]
      */

      const _currentPlayer = message.guild.uno.players.shift();
      message.guild.uno.players.push(_currentPlayer);
      // Skipped player isnt second in the queue anymore, they're first because the current player had to be looped back.
      const skippedPlayer = message.guild.uno.players.splice(0, 1)[0]; // .splice(0, 1) returns an array consisting of exactly 1 player.
      message.guild.uno.players.push(skippedPlayer);

      message.guild.uno.lastPlay = 'skip';
    }

    else if(response.value === 'reverse') {
      message.send('🔄 `|` 🃏 **Reverse!**');

      // REVERSE FUNCTIONALITY

      /* ❌ WRONG */
      // pre-reverse: [p1, p2, p3, p4]
      // shift & push: [p2, p3, p4, p1]
      // reverse: [p1, p4, p3, p2] 

      // If I did it like that, it would be p1's turn again.

      /* ❌ WRONG */
      // pre-reverse: [p1, p2, p3, p4]
      // reverse: [p4, p3, p2, p1]
      // shift & push: [p3, p2, p1, p4]

      // Also cant do that, because p4's turn would be skipped. So instead, i shift p1 *first*, THEN reverse.

      /* ✅ RIGHT */
      // pre-reverse: [p1, p2, p3, p4]
      // shift p1 off: p1 & [p2, p3, p4]
      // reverse: p1 & [p4, p3, p2]
      // push back on: [p4, p3, p2, p1]
  
      const _currentPlayer = message.guild.uno.players.shift();
      message.guild.uno.players.reverse();
      message.guild.uno.players.push(_currentPlayer);

      message.guild.uno.lastPlay = 'reverse';
    }

    else if(response.value === 'drawtwo') {
      message.send(`2️⃣🇽 \`|\` 🃏 **Draw two, ${await client.users.fetch(message.guild.uno.players[1].id)}!**`);

      const [newHand, newDeck] = addCards(message.guild.uno.players[1], 2, remainingDeck);
      message.guild.uno.players[1].hand = newHand;
      remainingDeck = newDeck;
      (await client.users.fetch(message.guild.uno.players[1].id)).send(`🃏 **Here's your hand!**\n${message.guild.uno.players[1].hand.map(card => `**\`${card.suit.toUpperCase()} ${card.value}\`**`).join(', ')}\n*You can play a card like this:* \`red 4\`\n*If you have a wild card, play it like this:* \`wild blue\` or \`wild4 blue\``).catch(catchDMAndRemove);

      // SEE SKIP FUNCTIONALITY FOR WHY THIS WORKS
      const _currentPlayer = message.guild.uno.players.shift();
      message.guild.uno.players.push(_currentPlayer);
      // Skipped player isnt second in the queue anymore, they're first because the current player had to be looped back.
      const drawTwodPlayer = message.guild.uno.players.splice(0, 1)[0]; // .splice(0, 1) returns an array consisting of exactly 1 player.
      message.guild.uno.players.push(drawTwodPlayer);

      message.guild.uno.lastPlay = 'drawtwo';
    }

    /****** END RULES SECTION ******/

    /****** BEGIN GAME PROGRESSION SECTION ******/

    if(message.guild.uno.lastPlay === 'invalid') {
      const response = await awaitReply(message, `❌ \`|\` 🃏 **Try again, ${playerUserObject.toString()}!**\n🃏 **Current card:** \`${formatCard(lastCard)}\n(You have 15 seconds)`, 15000, currentPlayer);

      client.logger.verbose('❌ INVALID PLAY RETRY');
      return game(response, currentPlayer, lastCard, remainingDeck);
    } else {
      const playedCard = currentPlayer.hand.find(g => g.suit === response.suit && g.value === response.value);
      currentPlayer.hand.splice(currentPlayer.hand.indexOf(playedCard), 1);

      playerUserObject.send(`🃏 **Here's your hand!**\n${currentPlayer.hand.map(card => `**\`${card.suit.toUpperCase()} ${card.value}\`**`).join(', ')}\n*You can play a card like this:* \`red 4\`\n*If you have a wild card, play it like this:* \`wild blue\` or \`wild4 blue\``).catch(async e => {
        if (e.message === 'Cannot send messages to this user') {
          message.guild.uno.players.splice(message.guild.uno.players.indexOf(currentPlayer), 1); // Remove the player from the queue
          message.send(`❌ \`|\` 🃏 ${(await client.users.fetch(currentPlayer.id)).toString()}, **I had to remove you from the game.**\nPlease enable your DMs so I can send you your cards, and rejoin later.`);
        } else {
          endGame('error');
          client.logger.error(e);
          message.send('❌ 🃏 **There was an error!** Please try again later.');
        }
      }); 

      if(!['drawtwo', 'wild', 'wild4', 'skip', 'reverse'].includes(message.guild.uno.lastPlay)) {
        const shiftedPlayer = message.guild.uno.players.shift();
        message.guild.uno.players.push(shiftedPlayer);
      }
    }

    if(message.guild.uno.players.length <= 1) {
      return endGame('winner');
    }

    currentPlayer = message.guild.uno.players[0];
    const _playerUserObject = await client.users.fetch(currentPlayer.id);

    lastCard = response;

    client.logger.verbose('✅ VALID PLAY');
    response = await awaitReply(message, `✅ \`|\` 🃏 **Your turn, ${_playerUserObject.toString()}!**\n🃏 **Current card:** \n${formatCard(lastCard)}\n(You have 15 seconds)`, 15000, currentPlayer);

    return game(response, currentPlayer, lastCard, remainingDeck);

    /****** END GAME PROGRESSION SECTION ******/
  }

  function endGame() {
    delete message.guild.uno;
    message.send('Game ended');
  }

  function addCards(player, cards, shuffledDeck) {
    for (let i = 0; i < cards; i++) {
      const randomCard = shuffledDeck.shift();
      player.hand.push(randomCard);
    }
    return [player.hand, shuffledDeck];
  }

  async function catchDMAndRemove(e) {
    if (e.message === 'Cannot send messages to this user') {
      message.guild.uno.players.splice(message.guild.uno.players.indexOf(message.guild.uno.players[1]), 1); // Remove the player from the queue
      message.send(`❌ \`|\` 🃏 ${(await client.users.fetch(message.guild.uno.players[1].id)).toString()}, **I had to remove you from the game.**\nPlease enable your DMs so I can send you your cards, and rejoin later.`);
    } else {
      endGame('error');
      client.logger.verbose(`From: ${__filename}`);
      client.logger.error(e);
      message.send('❌ 🃏 **There was an error!** Please try again later.');
    }
  }

}; // <-- end of module.exports.run. No seriously that's it.

function parseResponse(response) {
  if (!response) return null;

  // if the response is *already* a card object, return it.
  if(typeof response === 'object') return response;

  if(response.toLowerCase() === 'draw') return 'draw';

  response = response.toLowerCase() // Makes all lowercase
    .replace(/[^a-z0-9 ]/gi, '') // Gets rid of anything that's not a-z 0-9 or a space
    .split(' '); // Splits the response
  const suit = response[0];
  let value = response.slice(1); // => array
  if (!suit || !value) return false;
  value = value.join(' ') === 'draw two' ? 'drawtwo' : value.join(' ');

  const respond = { suit: undefined, value: undefined, wild: undefined, wild4: undefined };

  if (/^r(ed)?/i.test(suit)) respond.suit = 'red';
  else if (/^y(ellow)?/i.test(suit)) respond.suit = 'yellow';
  else if (/^g(reen)?/i.test(suit)) respond.suit = 'green';
  else if (/^b(lue)?/i.test(suit)) respond.suit = 'blue';
  else {
    if (suit === 'wild') {
      respond.wild = true;
      respond.suit = 'special';

      if (/^r(ed)?/i.test(value)) respond.value = 'red';
      else if (/^y(ellow)?/i.test(value)) respond.value = 'yellow';
      else if (/^g(reen)?/i.test(value)) respond.value = 'green';
      else if (/^b(lue)?/i.test(value)) respond.value = 'blue';
      else return false;
    } else if (suit === 'wild4') {
      respond.wild4 = true;
      respond.suit = 'special';

      if (/^r(ed)?/i.test(value)) respond.value = 'red';
      else if (/^y(ellow)?/i.test(value)) respond.value = 'yellow';
      else if (/^g(reen)?/i.test(value)) respond.value = 'green';
      else if (/^b(lue)?/i.test(value)) respond.value = 'blue';
      else return false;

    } else return false;
  }

  if (/[0-9]/i.test(+value)) respond.value = +value;
  else if (['drawtwo', 'reverse', 'skip'].includes(value)) respond.value = value;
  else return false;

  return respond;
}

/**
 * Formats the cards to look pretty in messages
 * DOES NOT HANDLE WILD CARDS!
 * @param {Object} card A card object
 * @returns {String} A colored Discord code block with card color and value
 */
function formatCard(card) {
  if (card.value === 'drawtwo') card.value = 'Draw Two';
  if (card.value === 'skip') card.value = 'Skip';
  if (card.value === 'reverse') card.value = 'Reverse';

  switch (card.suit) {
    case 'red': return `\`\`\`diff\n- RED ${card.value}\n\`\`\``;
    case 'yellow': return `\`\`\`fix\nYELLOW ${card.value}\n\`\`\``;
    case 'green': return `\`\`\`diff\n+ GREEN ${card.value}\n\`\`\``;
    case 'blue': return `\`\`\`md\n# BLUE ${card.value}\n\`\`\``;
    default: return false;
  }
}

// This is almost exactly the same as client.awaitReply, but with a modified filter.
// This filter checks if the author is the player.
// And the limit is 15 seconds instead of 30.
async function awaitReply(msg, question, limit = 15000, player) {
  const filter = m => m.author.id === player.id;
  await msg.channel.send(question);
  try {
    const collected = await msg.channel.awaitMessages(filter, { max: 1, time: limit, errors: ['time'] });
    return collected.first().content;
  } catch (e) {
    return false;
  }
}

function shuffle(array, size) {
  let index = -1;
  const length = array.length;
  const lastIndex = length - 1;

  size = size === undefined ? length : size;
  while (++index < size) {
    const rand = baseRandom(index, lastIndex),
      value = array[rand];

    array[rand] = array[index];
    array[index] = value;
  }
  array.length = size;
  return array;
}
function baseRandom(lower, upper) {
  return lower + Math.floor(Math.random() * (upper - lower + 1));
}

exports.conf = {
  enabled: false,
  aliases: [],
  permLevel: 'User',
  guildOnly: true
};

exports.help = {
  name: 'uno',
  description: 'Play a game of uno!',
  usage: 'uno <start/join/end/uno>',
  category: 'Fun'
};