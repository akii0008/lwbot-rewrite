const userFunc = require('../../dbFunctions/client/user');
module.exports.run = async (client, message) => {
  const User = new userFunc(message.author.id);
  User.changeBadges('add', ':sparkles:');

  if (!message.guild.me.permissions.has('MANAGE_NICKNAMES')) return message.send('❌ `|` :sparkles: **I do not have permission to manage nicknames!**');

  if (message.mentions.users.first()) {
    if (!message.member.permissions.has('MANAGE_NICKNAMES')) return message.send('❌ `|` :sparkles: **You do not have permission to change other people\'s nicknames!**');

    const targetMember = message.mentions.members.first();
    const targetUser = message.mentions.users.first();

    if (!targetMember.manageable) return message.send('❌ `|` :sparkles: **The member\'s role is higher than mine!**');
    if (targetMember.nickname && targetMember.nickname.includes('☆ ･*。')) return message.send('❌ `|` :sparkles: **That nickname already has a sparkle in it!**');

    if (targetMember.nickname && targetMember.nickname.length < 28) {
      await targetMember.setNickname(`☆ ･*。${targetMember.nickname}`);
      await message.send('✅ `|` :sparkles: **Nickname has been set!**');
    }
    else if (targetUser.username.length < 28) {
      await targetMember.setNickname(`☆ ･*。${targetUser.username}`);
      await message.send('✅ `|` :sparkles: **Nickname has been set!**');
    }
    else { message.send('❌ `|` :sparkles: **The user\'s nickname/username was too long! Please set it to something less than 28 characters!**'); }
  } else {
    if (!message.member.permissions.has('CHANGE_NICKNAME')) return message.send('❌ `|` :sparkles: **You do not have permission to change your nickname!**');

    const selfMember = message.member;
    const selfUser = message.author;

    if (!selfMember.manageable) return message.send('❌ `|` :sparkles: **Your role is higher than mine!**');
    if (selfMember.nickname && selfMember.nickname.includes('☆ ･*。')) return message.send('❌ `|` :sparkles: **You already have a sparkle in your nickname!**');

    if (selfMember.nickname && selfMember.nickname.length < 28) {
      await selfMember.setNickname(`☆ ･*。${selfMember.nickname}`);
      await message.send('✅ `|` :sparkles: **Your nickname was set!**');
    } else if (selfUser.username.length < 28) {
      await selfMember.setNickname(`☆ ･*。${selfUser.username}`);
      await message.send('✅ `|` :sparkles: **Your nickname was set!**');
    } else { message.send('❌ `|` :sparkles: **Your nickname/username was too long! Please set it to something less than 28 characters!**'); }
  }
};

exports.conf = {
  enabled: true,
  aliases: [],
  permLevel: 'User',
  guildOnly: true,
  hidden: true
};

exports.help = {
  name: 'sparkle',
  description: 'Make your nickname sparkly!',
  usage: 'sparkle [@user (MOD ONLY)]',
  category: 'Fun'
};