const Websocket = require('ws');
const port = 13807;
module.exports = (client) => {
  if (!client) return console.error('No client parameter passed.');

  // Create a new websocket server using the port defined above
  const wss = new Websocket.Server({ port });
  client.logger.log(`Websocket server opened on port ${port}`);

  // When a new client connects to the websocket
  wss.on('connection', (ws, req) => {
    client.logger.log(`New websocket connection! IP: ${req.connection.remoteAddress}`);
    const connectionTimer = setTimeout(() => ws.close(1001, 'Took too long to send identify and key'), 3000);

    // When the client sends a message to the websocket
    ws.on('message', message => {
      message = JSON.parse(message);

      client.logger.verbose('New websocket message:');
      client.logger.verbose(message);

      switch (message.action) {

        // Identify will be interesting because I don't actually check what the client is identifying *for*.
        // As long as they have the correct key, which will only be me as far as I'm concerned, I don't
        // care what they do.
        // Most likely there will be the failover client and the timer database connected, plus
        // anything else I may add later on.
        case 'identify': {
          if (message.key !== process.env.WEBSOCKET_KEY) return ws.close(1001, 'Invalid key');
          req.connection.isAuthenticated = true;

          ws.send(JSON.stringify({ action: 'identified' }));
          break;
        }

        case 'reconnect': {
          // No key check because it's gonna close anyway.
          client.logger.log('Websocket reconnected! Closing connection...', 'ready');
          ws.close(1003, 'Reconnection successful.');
          break;
        }

        case 'timerExpire': {
          if (!req.connection.isAuthenticated) return ws.close(1001, 'Not authenticated');
          break;
        }

        default: {
          return ws.close(1001, 'Invalid request');
        }
      }
      // once the switch case is done, clear the connection timer
      clearTimeout(connectionTimer);

    });

    ws.on('close', async (code, reason) => {
      if (code === 1006) {
        client.logger.warn('Websocket process ended! PMing owner now...');
        return (await client.users.fetch(client.config.ownerID)).send('⚠️ **Failover process closed.** Please check PM2.').catch(e => {
          if(e.message === 'Cannot send messages to this user') return client.logger.verbose('Failed to send DM to Akii');
          else client.logger.error(e);
        });
      }
      if (code === 1003) return; // Code 1003 = Reconnection successful
      client.logger.warn(`Websocket closed! Code: ${code} | Reason: ${reason}`);
    });
  });
};