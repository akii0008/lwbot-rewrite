const Websocket = require('ws');
const { join } = require('path');
require('dotenv').config({ path: join(__dirname, '../../.env') });

const port = process.env.PORT || 13807;
const ws = new Websocket(`ws://localhost:${port}`);

ws.on('open', () => {
  console.log('Connected. identifying...');
  ws.send(JSON.stringify({ action: 'identify', key: process.env.FAILOVER_WEBSOCKET_KEY }));
});

ws.on('message', message => {
  message = JSON.parse(message);

  if (message.action !== 'identified') return ws.close(4, 'Timer websocket did not authenticate properly');
});

// timer database interval thingy