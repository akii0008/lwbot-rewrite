module.exports = {
  apps: [{
    name: 'LWBot',
    script: './index.js',
    watch: false,
    env: {
      'FORCE_COLOR': 1,
      'NODE_ENV': 'production'
    },
    args: '-v'
  }]
};
