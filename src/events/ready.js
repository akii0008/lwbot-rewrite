const { Collection } = require('discord.js');
const moment = require('moment');
const watchdog = require('../util/sqWatchdog');
const WSS = require('../websocket/ws-server');
const Websocket = require('ws');
const fetch = require('node-fetch');
const brain = require('brain.js');
const { readdirSync } = require('fs');
const { readFile } = require('fs').promises;
const { join } = require('path');
const { statuses, enabled } = require('../util/statuses');
const { version } = require('../../package.json');

module.exports = async client => {
  if (!client.user.bot) {
    client.logger.error(`The user I logged in with, ${client.user.tag}, is not a bot!`);
    client.logger.error('This is strictly against the Discord Terms of Service!');
    client.logger.error('• http://discordapp.com/terms');
    client.logger.error('I will now exit FOR YOUR OWN SAFETY! I am protecting your account from getting banned!');
    process.exit(1);
  }

  require('../dbFunctions/client/protos.js')(client);

  client.logger.verbose(`
  ______ ______  _____  _____  _   _    _____  _____   ______   ___   _   _
  |  ___|| ___ \\|  _  ||_   _|| | | |  |_   _||_   _|  |  _  \\ / _ \\ | \\ | |
  | |_   | |_/ /| | | |  | |  | |_| |    | |    | |    | | | |/ /_\\ \\|  \\| |
  |  _|  |    / | | | |  | |  |  _  |    | |    | |    | | | ||  _  || . ' |
  | |    | |\\ \\ \\ \\_/ /  | |  | | | |   _| |_   | |    | |/ / | | | || |\\  |
  \\_|    \\_| \\_| \\___/   \\_/  \\_| |_/   \\___/   \\_/    |___/  \\_| |_/\\_| \\_/

  `);

  // If the bot isn't in failover mode, start the status rotation and start updating DBots info
  if (!global.failover) {
    client.logger.debug('Starting status rotation...');
    client.statusRotationInterval = setInterval(() => {
      if (!enabled) return false;
      const randomPl = statuses(client).randomElement();
      client.logger.debug(`Setting status to ${randomPl[1].type} ${randomPl[0]}`);
      return client.user.setActivity(`${randomPl[0]} | !w help`, randomPl[1]);
    }, 60000);

    // Update guild count on discord.bots.gg
    setInterval(() => {
      if (process.env.DBOTS_KEY === '') return client.logger.debug('Didn\'t update DBots page');
      fetch('https://discord.bots.gg/api/v1/bots/377205339323367425/stats', {
        method: 'POST',
        body: JSON.stringify({ guildCount: client.guilds.cache.size }),
        headers: {
          'Authorization': process.env.DBOTS_KEY,
          'Content-Type': 'application/json'
        }
      })
        .then(res => res.json())
        .then(res => client.logger.verbose(res))
        .then(() => client.logger.debug('Updated DBots page with current guild count'))
        .catch(e => {
          client.logger.error(`Something went wrong updating the DBots page: ${e}`);
          client.logger.verbose(e);
        });
    }, 60000); // Every minute (ratelimit is 20/second)
  }

  if (!client.config.ciMode) {
    // Finds if there was an error generated on uncaughtException the last time the bot started up.
    // This is achieved by writing a new file on error, exiting, then on restart, reading the file
    // then sending the contents to me.
    try {
      const fs = require('fs-extra');
      let e = require.resolve('../e');
      e = await fs.readFileSync('e', 'utf8');
      await client.users.fetch(client.config.ownerID).send(`**I restarted! There was an error before I restarted:**\n\`\`\`${e}\`\`\``);
      client.logger.log('Error log reported.');
    } catch (e) {
      if (e.code === 'MODULE_NOT_FOUND') client.logger.debug('No error log found.');
      else client.logger.error(e.stack);
    }

    // Initialize the brains
    const brains = readdirSync('brains/');
    const brainsWithoutJson = brains.map(g => g.split('.json')[0]);
    client.guilds.cache.forEach(async guild => {
      guild.brain = new brain.recurrent.LSTM({ hiddenLayers: [20, 20, 20] });

      if (brainsWithoutJson.includes(guild.id)) {
        const brainData = JSON.parse((await readFile(`${join(__dirname, '../brains')}/${guild.id}.json`)).toString()); // Buffer -> String -> Object
        client.logger.verbose(`Loading saved brain for guild ${guild.id}`);
        return guild.brain.fromJSON(brainData);
      }
    });
  }

  // Define all client variables before they're called elsewhere
  client.settings = new Collection();
  client.events = new Collection();
  client.msgCmdHistory = new Collection();
  client.musicQueue = new Collection();

  // Run the watchdog once before ready
  client.logger.log('Running watchdog once before full startup...');
  const wasSqLogEnabled = client.config.sqLogMode; // boolean
  if (!wasSqLogEnabled) { client.logger.log('Enabling sqLogMode temporarily while it runs...'); client.config.sqLogMode = true; }
  if (client.config.ciMode) await watchdog.runner(client, await client.guilds.fetch('332632603737849856'));
  else await watchdog.runner(client);
  if (!wasSqLogEnabled) { client.logger.log('Disabling sqLogMode because it was disabled originally...'); client.config.sqLogMode = false; }

  // Start the sqWatchdog interval timer
  watchdog.timer(client);

  if (!global.failover) { // If this is the main process (aka not failover mode)
    // Create the websocket server, and pass the client in
    WSS(client);
    // This is handled in another file because it would clutter this one.
  } else { // If this is the failover process (aka failover mode)
    client.logger.warn('Failover mode enabled. Websocket server not started.');

    const reconnectionTimer = setInterval(reconnect, 1000);
    function reconnect() { // eslint-disable-line no-inner-declarations
      const ws = new Websocket('ws://localhost:13807');

      // Websocket connection encountered an error.
      ws.on('error', err => {
        if (err.code === 'ECONNREFUSED') { client.logger.verbose('Main process reconnection failed. Assuming it is still offline. Retrying...'); }
        else {
          client.logger.error(`FAILOVER ERROR: ${err}\n\nPROCESS WILL NOW EXIT`);
          process.exit(1);
        }
      });

      // Reconnection established!
      ws.on('open', () => {
        client.logger.log('Main process reconnected!', 'ready');
        ws.send(JSON.stringify({ action: 'reconnect' }));
        clearInterval(reconnectionTimer);
        // Don't exit the process here because the server will close it for me
      });

      // Connection was closed.
      ws.on('close', (code, reason) => {
        if (code === 1006) return; // Code 1006 = Connection does not exist
        client.logger.log(`Failover websocket connection closed gracefully. \nCode: ${code} | Reason: ${reason}`);

        if (process.env.pm_uptime) {
          client.logger.log('Restarting using PM2...');
          process.exit();
        } else {
          client.logger.warn('Process is not running via PM2. Manual restart required.');
          process.exit();
        }
      });
    }

  }

  client.ready = true;

  const after = new Date();
  client.startup = after - client.before;

  if (client.config.debugMode) client.logger.debug('Debug mode enabled');
  if (client.config.verboseMode) client.logger.verbose('Verbose mode enabled');
  if (client.config.sqLogMode) client.logger.sqLog('SQLog mode enabled');
  client.logger.log(`
${'⎻'.repeat(client.user.tag.length + client.user.id.length + version.length + 7)}
 ${client.user.tag} (${client.user.id}) v${version}
${'⎼'.repeat(client.user.tag.length + client.user.id.length + version.length + 7)}
• Users:     ${client.users.cache.size}
• Guilds:    ${client.guilds.cache.size}
• Channels:  ${client.channels.cache.size}
• Took:      ${moment.duration(client.startup, 'milliseconds').format('[~]s [secs]')} to start up`, 'ready');
  if (global.failover) { client.logger.warn('RUNNING IN FAILOVER MODE'); client.logger.warn('SOME FUNCTIONALITY HAS BEEN DISABLED'); }


  delete client.before;
  delete client.after;
  delete client.startup;

  if (client.config.ciMode) client.emit('ciStepGuildCreate');
};