const { version } = require('../../package.json');
module.exports.statuses = client => [
  /* Playing */
  ['with James', { type: 'PLAYING' }],
  ['with Nen', { type: 'PLAYING' }],
  ['with Gallium', { type: 'PLAYING' }],
  ['with fire', { type: 'PLAYING' }],
  ['on Webtoons instead of working', { type: 'PLAYING' }],
  ['with your heart', { type: 'PLAYING' }],
  ['with Shen', { type: 'PLAYING' }],
  ['with Shenpai', { type: 'PLAYING' }],
  ['with SAI', { type: 'PLAYING' }],
  ['some game or something idrk', { type: 'PLAYING' }],
  ['with the big boys', { type: 'PLAYING' }],
  ['with Madi', { type: 'PLAYING' }],
  ['in Webtoonland', { type: 'PLAYING' }],
  ['in Wonderland', { type: 'PLAYING' }],
  ['Adobe Illustrator', { type: 'PLAYING' }],
  ['Fire Alpaca', { type: 'PLAYING' }],
  ['for the money', { type: 'PLAYING' }],
  ['with my code', { type: 'PLAYING' }],
  ['with time', { type: 'PLAYING' }],
  ['in space', { type: 'PLAYING' }],
  ['for the good guys', { type: 'PLAYING' }],
  ['with other bots', { type: 'PLAYING' }],
  ['with the ratelimit ;)', { type: 'PLAYING' }],
  ['with the Podcast crew', { type: 'PLAYING' }],
  ['[status]', { type: 'PLAYING' }],
  ['TODO: Add witty status', { type: 'PLAYING' }],
  ['[object Object]', { type: 'PLAYING' }],
  ['against the clock', { type: 'PLAYING' }],
  ['Error 404: Not Found', { type: 'PLAYING' }],
  ['Error 418: I\'m a teapot', { type: 'PLAYING' }],
  ['with your ships', { type: 'PLAYING' }],
  ['Monopoly', { type: 'PLAYING' }],
  ['with life in a box', { type: 'PLAYING' }],
  ['with life', { type: 'PLAYING' }],
  ['with the other lurkers', { type: 'PLAYING' }],
  ['with the skin of my enemies', { type: 'PLAYING' }],
  ['for the glory', { type: 'PLAYING' }],
  ['with friends', { type: 'PLAYING' }],
  ['on the beach', { type: 'PLAYING' }],
  ['at the mall', { type: 'PLAYING' }],
  ['at home', { type: 'PLAYING' }],
  ['on the couch', { type: 'PLAYING' }],
  ['?¿', { type: 'PLAYING' }],
  ['devil\'s advocate', { type: 'PLAYING' }],
  ['Poker', { type: 'PLAYING' }],
  ['MS Paint', { type: 'PLAYING' }],
  ['with Cubits', { type: 'PLAYING' }],
  ['with Uru-chan', { type: 'PLAYING' }],
  ['with Quimchee', { type: 'PLAYING' }],
  [' ', { type: 'PLAYING' }],
  ['nothing.. b-baka!! (つω⊂* ) Why do you care?? Hmph! (/□＼*)・゜', { type: 'PLAYING' }],
  ['hl3_beta-B1', { type: 'PLAYING' }],
  ['on a Raspberry Pi', { type: 'PLAYING' }],
  [`with ${client.users.cache.size} users!`, { type: 'PLAYING' }],
  [`with ${client.guilds.cache.size} servers!`, { type: 'PLAYING' }],
  ['with da bois', { type: 'PLAYING' }],
  [`on version v${version} :)`, { type: 'PLAYING' }],
  ['nichts', { type: 'PLAYING' }],

  /* Watching */
  ['Netflix', { type: 'WATCHING' }],
  ['you', { type: 'WATCHING' }],
  ['you sleep', { type: 'WATCHING' }],
  ['an unhealthy amount of anime', { type: 'WATCHING' }],
  [`${client.users.cache.size} users...`, { type: 'WATCHING' }],
  [`${client.guilds.cache.size} servers...`, { type: 'WATCHING' }],
  [`${client.channels.cache.size} channels...`, { type: 'WATCHING' }],

  /* Listening to */
  ['Spotify', { type: 'LISTENING' }],
  ['your conversations-- I mean what', { type: 'LISTENING' }],
  [`${client.users.cache.size} users-- wait I'm not allowed to oops`, { type: 'LISTENING' }],

  /* Competing in */
  ['a battle between life and death', { type: 'COMPETING' }],
  ['the Thunderdome', { type: 'COMPETING' }]
];

module.exports.enabled = true;
