module.exports.prettyPerms = {
  ADMINISTRATOR:         'Administrator',
  CREATE_INSTANT_INVITE: 'Create Instant Invite',
  KICK_MEMBERS:          'Kick Members',
  BAN_MEMBERS:           'Ban Members',
  MANAGE_CHANNELS:       'Manage Channels',
  MANAGE_GUILD:          'Manage Guild',
  ADD_REACTIONS:         'Add Reactions',
  VIEW_AUDIT_LOG:        'View Audit Log',
  PRIORITY_SPEAKER:      'Priority Speaker',
  READ_MESSAGES:         'Read Messages',
  SEND_MESSAGES:         'Send Messages',
  SEND_TTS_MESSAGES:     'Send TTS Messages',
  MANAGE_MESSAGES:       'Manage Messages',
  EMBED_LINKS:           'Embed Links',
  ATTACH_FILES:          'Attach Files',
  READ_MESSAGE_HISTORY:  'Read Message History',
  MENTION_EVERYONE:      'Mention Everyone',
  USE_EXTERNAL_EMOJIS:   'Use External Emojis',
  CONNECT:               'Connect',
  SPEAK:                 'Speak',
  MUTE_MEMBERS:          'Mute Member',
  DEAFEN_MEMBERS:        'Deafen Members',
  USE_VAD:               'Use Voice Activation Detection',
  CHANGE_NICKNAME:       'Change Nickname',
  MANAGE_ROLES:          'Manage Roles',
  MANAGE_WEBHOOKS:       'Manage Webhooks',
  MANAGE_EMOJIS:         'Manage Emojis',
  STREAM:                'Stream'
};

module.exports.prettyFeatures = {
  ANIMATED_ICON: 'Animated Icon',
  BANNER:        'Banner',
  COMMERCE:      'Commerce',
  DISCOVERABLE:  'Discoverable',
  FEATURABLE:    'Featurable',
  INVITE_SPLASH: 'Invite Splash',
  NEWS:          'News',
  PARTNERED:     'Partnered',
  RELAY_ENABLED: 'Relay Enabled',
  VANITY_URL:    'Vanity URL',
  VERIFIED:      'Verified',
  VIP_REGIONS:   'VIP Regions',
  WELCOME_SCREEN_ENABLED: 'Welcome Screen Enabled'
};

module.exports.prettyPremiumTiers = {
  0: 'None',
  1: 'Tier 1',
  2: 'Tier 2',
  3: 'Tier 3'
};

module.exports.prettyEvents = {
  messageDelete: 'Message Delete',
  messageUpdate: 'Message Edit',
  channelCreate: 'Channel Create',
  channelDelete: 'Channel Delete',
  guildMemberAdd: 'Member Joined',
  guildMemberRemove: 'Member Left'
};