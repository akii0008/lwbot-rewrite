const { MessageEmbed, User, GuildMember } = require('discord.js');
module.exports = async (client, message) => {

  // Message send function, pretty much extends message.channel.send/message.edit in that it allows the user to edit their command message and it runs that instead
  message.send = (content, options) => {
    // If in failover mode, effectively disable command editing functionality.
    if (global.failover) {
      // Just use the vanilla method instead of the modded one, which could cause issues
      return message.channel.send(content, options);
    }

    return new Promise((resolve, reject) => {
      if (!content) { // Accidental empty message handling
        content = '[Empty Message]';
        reject('Empty message detected!');
      }

      // command editing functionality
      if (message.edited) return message.channel.messages.fetch(client.msgCmdHistory[message.id])
        .then(async msg => {
          const embedC = content instanceof MessageEmbed; // embedC = "embed Check"

          options = {
            embed: embedC
              ? content
              : msg.embeds.length !== 0
                ? null : null,
            code: options
              ? options.code
                ? options.code
                : undefined
              : undefined,
            split: options
              ? options.split
                ? options.split
                : undefined
              : undefined
          };

          if (msg.reactions.size > 0) msg.reactions.removeAll();

          return msg.edit(embedC ? '' : content, options) // If content === object, send (text) nothing. Else, send the content
            .then(m => { return resolve(m); })
            .catch(e => { return reject(e); });
        })
        .catch(e => {
          if (e.message === 'Unknown Message') {
            message.channel.send(format(client, message, content), options ? options : null)
              .then(msg => {
                client.msgCmdHistory[message.id] = msg.id;
                return resolve(msg);
              })
              .catch(e => { return reject(new Error(e)); });
          } else return reject(new Error(e));
        });

      // Checks if msgCmdHistory (Set) has the id of the message. If not, it sends a new message.
      // This is placed after the initial edit statement because it will send a message regardless.
      if (!client.msgCmdHistory.has(message.id)) {
        if (options) return message.channel.send(format(client, message, content), options).then(msg => {
          client.msgCmdHistory[message.id] = msg.id;
          return resolve(msg);
        }).catch(e => { return reject(new Error(e)); });
        else return message.channel.send(format(client, message, content)).then(msg => {
          client.msgCmdHistory[message.id] = msg.id;
          return resolve(msg);
        }).catch(e => { return reject(new Error(e)); });
      }
    });
  };

  // Other various functions
  message.functions = {
    /**
     * Parses a role from a given role name or role snowflake
     * @param {String} data
     * @returns {Role}
     */
    parseRole: async data => {
      if (message.channel.type !== 'text') throw new Error('I can\'t find a role if I\'m not in a guild!');
      if (!data) throw new Error('You didn\'t give me anything to find a role from!');
      if (message.mentions.roles.size === 0) {
        let role = await message.guild.roles.fetch(data);
        if (!role) {
          role = message.guild.roles.cache.find(r => r.name.toLowerCase().includes(data.toLowerCase()));
          if (!role) throw new Error('I couldn\'t find that role! ');
          else return role;
        } else return role;
      } else {
        const role = message.mentions.roles.first();
        return role;
      }
    },
    /**
     * Parses a user from a given user name or user snowflake
     * @param {String} data
     * @returns {User}
     */
    parseUser: async data => {
      if (!data) throw new Error('You didn\'t give me anything to find a user from!');
      if (data instanceof User) return data;
      if (message.mentions.users.size === 0) {
        let user = await client.users.fetch(data);
        if (!user) {
          user = client.users.cache.find(r => (r.username.toLowerCase().includes(data.toLowerCase()) || r.tag.toLowerCase() === data.toLowerCase()));
          if (!user) throw new Error('I couldn\'t find that user!');
          else return user;
        } else return user;
      } else {
        const user = message.mentions.users.first();
        return user;
      }
    },
    /**
     * Parses a member from a given member username or member snowflake
     * @param {String} data
     * @returns {GuildMember}
     */
    parseMember: async data => {
      if (message.channel.type !== 'text') throw new Error('I can\'t find a member if I\'m not in a guild!');
      if (!data) throw new Error('You didn\'t give me anything to find a member from!');
      if (data instanceof GuildMember) return data;
      if (message.mentions.members.size === 0) {
        let member = await message.guild.members.fetch(data);
        if (!member) {
          member = message.guild.members.cache.find(r => (r.user.username.toLowerCase().includes(data.toLowerCase()) || r.user.tag.toLowerCase() === data.toLowerCase()));
          if (!member) throw new Error('I couldn\'t find that member!');
          else return member;
        } else return member;
      } else {
        const member = message.mentions.members.first();
        return member;
      }
    },
    /**
     * Parses a channel from a given channel name or channel snowflake
     * CANNOT PARSE DMS
     * @param {String} data
     * @returns {GuildChannel}
     */
    parseChannel: data => {
      if (!message.guild) throw new Error('I can\'t find a channel if I\'m not in a guild!');
      if (!data) return null;
      if (data.startsWith('<#') && data.endsWith('>')) { // data === <#ID>
        const channel = message.guild.channels.cache.get(data.substring(2, data.length - 1));
        if (!channel) throw new Error('Channel does not exist');
        return channel;
      }
      else if (data.startsWith('#')) { // data === #channel
        data = data.substring(1);
        const channel = message.guild.channels.cache.find(g => g.name.includes(data));
        if (!channel) throw new Error('Channel does not exist');
        return channel;
      }
      else { // data === ID
        const channel = message.guild.channels.cache.get(data);
        if (!channel) throw new Error('Channel does not exist');
        return channel;
      }
    }
  };
};

function format(client, message, content) {
  const owoedContent = owoify(client, message, content); // Will return the plain content if owo mode isnt on
  const emojifiedContent = emojify(client, message, owoedContent); // Will return the plain content if emojify mode isnt on

  return emojifiedContent; // Possible outcomes: owoed, emojified, owoed + emojified, normal
}

function owoify(client, message, content) {
  // owo mode functionality
  if (!(message.guild && client.settings.get(message.guild.id)['owoMode'] === 'true')) return content;
  if (content instanceof MessageEmbed) {
    const owoedEmbed = new MessageEmbed();

    // Stuff that's changed
    if (content.title) owoedEmbed.setTitle(owoedContent(content.title));
    if (content.description) owoedEmbed.setDescription(owoedContent(content.description));
    if (content.author) owoedEmbed.setAuthor(
      owoedContent(content.author.name), // Set author name
      content.author.iconURL ? content.author.iconURL : undefined, // Set author icon
    );
    if (content.footer) owoedEmbed.setFooter(owoedContent(content.footer.text));
    if (content.fields) for (const field of content.fields) { owoedEmbed.addField(field.name, owoedContent(field.value), field.inline); }

    // Stuff that's not changed, but still needs to be transferred over
    if (content.color) owoedEmbed.setColor(content.color);
    if (content.url) owoedEmbed.setURL(content.url);
    if (content.timestamp) owoedEmbed.setTimestamp(content.timestamp);
    if (content.files.length !== 0) owoedEmbed.attachFiles(content.files); // MessageEmbed.files is an array

    return owoedEmbed;
  }

  return owoedContent(content);

}

function owoedContent(str) {
  str = str.replace(/r/g, 'w').replace(/l/g, 'w').replace(/R/g, 'W').replace(/L/g, 'W');
  const ous = ['o', 'O', '0', 'u', 'U'];
  const ws = ['w', 'W'];

  return str += ` ${[`${ous.randomElement()}${ws.randomElement()}${ous.randomElement()}`, ':3c'].randomElement()}`;
}



function emojify(client, message, content) {
  // emojify mode functionality
  if (!(message.guild && client.settings.get(message.guild.id)['emojifyMode'] === 'true')) return content;

  if (content instanceof MessageEmbed) {
    const emojiedEmbed = new MessageEmbed();

    // Stuff that's changed
    if (content.title) emojiedEmbed.setTitle(emojiedContent(content.title));
    if (content.description) emojiedEmbed.setDescription(emojiedContent(content.description));
    if (content.author) emojiedEmbed.setAuthor(
      emojiedContent(content.author.name), // Set author name
      content.author.iconURL ? content.author.iconURL : undefined, // Set author icon
    );
    if (content.footer) emojiedEmbed.setFooter(emojiedContent(content.footer.text));
    if (content.fields) for (const field of content.fields) { emojiedEmbed.addField(field.name, emojiedContent(field.value), field.inline); }

    // Stuff that's not changed, but still needs to be transferred over
    if (content.color) emojiedEmbed.setColor(content.color);
    if (content.url) emojiedEmbed.setURL(content.url);
    if (content.timestamp) emojiedEmbed.setTimestamp(content.timestamp);
    if (content.files.length !== 0) emojiedEmbed.attachFiles(content.files); // MessageEmbed.files is an array

    return emojiedEmbed;
  }

  return emojiedContent(content);

}

const cursedEmojis = ['😳', '🙈', '🥴', '👀', '🥵', '👈', '😡', '📲', '😂', '😏', '😤', '😜', '💯', '🤬', '🅱️', '🥺', '😋', '😈', '😍'];
function emojiedContent(str) {
  let msg = '';
  for (const part of str.split(' ')) {
    msg += `${part} ${cursedEmojis.randomElement()} `;
  }

  if (msg.length > 2000) msg = msg.substring(0, 2000); // Truncate the message

  return msg;
}