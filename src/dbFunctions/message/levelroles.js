const Sequelize = require('sequelize');
const { Guild } = require('discord.js'); // eslint-disable-line no-unused-vars
const client = require('../../startup').client;

class GuildLevelRoles {
  constructor(guildID) {
    if (guildID instanceof Guild) guildID = guildID.id; // If the guildID passed is actually a d.js guild object, turn it into an id that the class can handle.

    const guild = client.guilds.cache.get(guildID);
    if (!guild) throw new Error(`Guild (${guildID}) does not exist to get.`);

    this.guildID = guildID;
    this.guildObject = guild;
  }

  /**
   * The table of the guild settings
   */
  get table() {
    return new Sequelize('database', 'user', 'password', {
      host: 'localhost',
      dialect: 'sqlite',
      logging: false,
      storage: `databases/servers/${this.guildID}.sqlite`,
      transactionType: 'IMMEDIATE', // Setting this helps with the "SQLITE_BUSY: Database is locked" errors
      pool: {
        max: 1,
        min: 0
      }
    });
  }

  /**
   * The schema used to poll the database table
   */
  get guildSchema() {
    return this.table.define('levelrole', {
      role: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      level: {
        type: Sequelize.STRING,
        allowNull: false
      }
    }, { timestamps: false });
  }

  /**
   * Simple shortcut to the table using the guild ID provided earlier
   */
  get shortcut() {
    if (!this.guildObject.syncedDBs) {
      this.guildObject.syncedDBs = { levelroles: true };
      this.guildSchema.sync();
    } else if (!this.guildObject.syncedDBs.levelroles) {
      this.guildObject.syncedDBs.levelroles = true;
      this.guildSchema.sync();
    }

    return this.guildSchema;
  }

  addRole(roleID, level) {
    return new Promise((resolve, reject) => {
      if (!roleID) return reject(new Error('Missing role id'));
      if (typeof roleID !== 'string') return reject(new TypeError('roleID is not a string'));
      if (!level) return reject(new Error('Missing level'));
      if (typeof level !== 'number') return reject(new TypeError('level is not a number'));

      return this.shortcut.findOrCreate({ where: { role: roleID, level } })
        .then(() => resolve({
          role: roleID,
          level
        })).catch(reject);
    });
  }

  removeRole(roleID) {
    return new Promise((resolve, reject) => {
      if (!roleID) return reject(new Error('Missing role id'));
      if (typeof roleID !== 'string') return reject(new TypeError('roleID is not a string'));

      return this.shortcut.destroy({ where: { role: roleID } })
        .then(res => {
          if (res === 0) return reject(new Error('Role did not exist to remove'));
          else return resolve(true);
        }).catch(reject);
    });
  }
  deleteRole(roleID) { return this.removeRole(roleID); } // an alias because i will likely mess this up at some point

  editRole(roleID, newLevel) {
    return new Promise((resolve, reject) => {
      if (!roleID) return reject(new Error('Missing role id'));
      if (typeof roleID !== 'string') return reject(new TypeError('roleID is not a string'));
      if (!newLevel) return reject(new Error('Missing new level to set role to'));
      if (typeof newLevel !== 'number') return reject(new TypeError('newLevel is not a number'));

      return this.shortcut.update({ level: newLevel }, { where: { role: roleID } })
        .then(() => resolve({
          role: roleID,
          level: newLevel
        })).catch(reject);
    });
  }

  getRoles() {
    return new Promise((resolve, reject) => {
      return this.shortcut.findAll({}).then(data => {
        if (data.length === 0) return resolve(null);

        if(!this.guildObject.levelRoles) this.guildObject.levelRoles = new Map();
        const roleArray = [];
        for (const role of data) {
          roleArray.push({ role: role.role, level: role.level });
          this.guildObject.levelRoles.set(role.level, role.role);
        }

        return resolve(roleArray);
      }).catch(reject);
    });
  }
}

module.exports = GuildLevelRoles;